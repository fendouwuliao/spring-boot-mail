
package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
@RestController
public class DemoApplication {

    @Autowired
	private JavaMailSender mailSender;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@GetMapping("/")
	public String send() {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("ss88817@163.com");
		message.setTo("ss888171@163.com");
		message.setSubject("test email from serverless function");
		message.setText("test email from serverless function --- content body");
		mailSender.send(message);
        return "send ok";
	}

}
